package com.medicamentul.medicamentul.exception;

public enum EntityType {
    USER,
    ROLE
}