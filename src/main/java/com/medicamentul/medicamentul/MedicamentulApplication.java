package com.medicamentul.medicamentul;

import com.medicamentul.medicamentul.model.user.Role;
import com.medicamentul.medicamentul.model.user.User;
import com.medicamentul.medicamentul.model.user.UserRoles;
import com.medicamentul.medicamentul.repository.user.RoleRepositoryDao;
import com.medicamentul.medicamentul.repository.user.UserRepositoryDao;
import com.medicamentul.medicamentul.service.medicineService.MedicineServiceImpl;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.io.IOException;
import java.util.Arrays;


//This is the Spring Boot main class that bootstraps the project when the jar is executed
@SpringBootApplication
public class MedicamentulApplication {


    //The method that runs the application when the jar is executes
    public static void main(String[] args) throws IOException {

        SpringApplication.run(MedicamentulApplication.class, args);

//        MedicineServiceImpl medicineService = new MedicineServiceImpl();
//        System.out.println(medicineService.getMedicines());
    }

//        @Bean
//        CommandLineRunner init (RoleRepositoryDao roleRepository, UserRepositoryDao userRepository){
//            return args -> {
//
//                //Create Admin and Passenger Roles
//                Role adminRole = roleRepository.findByRole(UserRoles.ADMIN);
//                if (adminRole == null) {
//                    adminRole = new Role();
//                    adminRole.setRole(UserRoles.ADMIN);
//                    roleRepository.save(adminRole);
//                }
//
//                Role userRole = roleRepository.findByRole(UserRoles.USER);
//                if (userRole == null) {
//                    userRole = new Role();
//                    userRole.setRole(UserRoles.USER);
//                    roleRepository.save(userRole);
//                }
//
//                //Create an Admin user
//                User admin = userRepository.findByEmail("admin@gmail.com");
//                if (admin == null) {
//                    admin = new User()
//                            .setEmail("admin@gmail.com")
//                            .setPassword("$2a$10$7PtcjEnWb/ZkgyXyxY1/Iei2dGgGQUbqIIll/dt.qJ8l8nQBWMbYO") // "123456"
//                            .setFirstName("John")
//                            .setLastName("Doe")
//                            .setRoles(Arrays.asList(adminRole));
//                    userRepository.save(admin);
//                }
//
//                //Create a passenger user
//                User passenger = userRepository.findByEmail("passenger@gmail.com");
//                if (passenger == null) {
//                    passenger = new User()
//                            .setEmail("passenger@gmail.com")
//                            .setPassword("$2a$10$7PtcjEnWb/ZkgyXyxY1/Iei2dGgGQUbqIIll/dt.qJ8l8nQBWMbYO") // "123456"
//                            .setFirstName("Mira")
//                            .setLastName("Jane")
//                            .setAge(24)
//                            .setRoles(Arrays.asList(userRole));
//                    userRepository.save(passenger);
//                }
//            };
//        }
    }

