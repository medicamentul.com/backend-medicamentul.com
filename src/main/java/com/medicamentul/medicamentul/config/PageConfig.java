package com.medicamentul.medicamentul.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;


/**
 * Let Spring scan and configure @Controller annotated classes, to
 * configure component scanning on packages where controllers are stored.
  **/
@Configuration
public class PageConfig implements WebMvcConfigurer {

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/").setViewName("home");
        registry.addViewController("/login").setViewName("login");
        registry.addViewController("/signup").setViewName("signup");
        registry.addViewController("/dashboard").setViewName("dashboard");
        registry.addViewController("/logout").setViewName("logout");
        registry.addViewController("excel/upload").setViewName("upload");
        registry.addViewController("excel/upload_medical_devices").setViewName("upload_medical_devices");
        registry.addViewController("/search_medical_device").setViewName("search_medical_device");
        registry.addViewController("/medicines").setViewName("medicines");
        registry.addViewController("/search").setViewName("search");
    }

}