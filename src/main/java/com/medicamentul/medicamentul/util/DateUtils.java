package com.medicamentul.medicamentul.util;

import lombok.Getter;
import lombok.NoArgsConstructor;

import java.text.SimpleDateFormat;
import java.util.Date;


//In this class I will use the Singleton Pattern, because I will need only an object of it
@Getter
@NoArgsConstructor
public class DateUtils {

    private static DateUtils dateUtil = new DateUtils();

    private static final SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

    /**
     * Returns today's date as java.util.Date object
     *
     * @return today's date as java.util.Date object
     */
    public static Date today() {
        return new Date();
    }

    /**
     * Returns today's date as yyyy-MM-dd format
     *
     * @return today's date as yyyy-MM-dd format
     */
    public static String todayStr() {
        return sdf.format(today());
    }

    /**
     * Returns the formatted String date for the passed java.util.Date object
     *
     * @param date
     * @return
     */
    public static String formattedDate(Date date) {
        return date != null ? sdf.format(date) : todayStr();
    }

}

