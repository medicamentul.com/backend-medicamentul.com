package com.medicamentul.medicamentul.model.user;

import lombok.*;
import lombok.experimental.Accessors;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@ToString
@EqualsAndHashCode
@Accessors(chain = true)
@Entity(name = "user_geolocation")
public class UserGeolocation {

    @Id
    @Column(name = "user_id")
    private long id;

    private String ipAddress;
    private Double lat;
    private Double lon;
    private String city;


    @OneToOne
    @MapsId
    @JoinColumn(name = "user_id")
    private User user;
}
