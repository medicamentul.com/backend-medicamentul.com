package com.medicamentul.medicamentul.model.user;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

//@Getter
//@NoArgsConstructor
//@Accessors(chain = true)
public enum UserRoles {
     USER, ADMIN
}

