package com.medicamentul.medicamentul.model.user;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.util.Collection;
import java.util.Date;



@Getter
@Setter
@NoArgsConstructor
@Accessors(chain = true)
@Entity
@Table(name = "login")
public class LogIn {

    @Id
    @Column(name = "user_id")
    private Long id;

    private Date dateUserCreated;
    private Date dateUserLastLogIn;
    private Date dateUserLastLogOut;
    private Date accountLock;
    private String ipAddress;


    @OneToOne
    @MapsId
    @JoinColumn(name = "user_id")
    private User user;


}
