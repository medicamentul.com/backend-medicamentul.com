package com.medicamentul.medicamentul.model.user;

import com.medicamentul.medicamentul.model.pharmacy.Pharmacy;
import com.medicamentul.medicamentul.model.pharmacy.PharmacyGeolocation;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@Accessors(chain = true)
@Entity
@Table(name = "user",
        indexes = @Index(
                name = "idx_user_email",
                columnList = "email",
                unique = true
        ))
public class User {
//    @Id
//    @GeneratedValue(strategy= GenerationType.AUTO, generator="native")
//    @GenericGenerator(name = "native", strategy = "native")
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    @NotBlank(message = "Name should not be blank")
    private String firstName;
    @NotBlank(message = "Last Name should not be blank")
    private String lastName;
    @NotNull(message = "Age should not be blank")
    private Integer age;
//    @NotBlank(message = "Username should not be blank")
//    private String username;
    @NotBlank(message = "Email should not be blank")
    @Column(nullable = false, unique = true)
    private String email;
    @NotBlank(message = "Password should not be blank")
    private String password;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "user_role",
            joinColumns = {@JoinColumn(name = "user_id")},
            inverseJoinColumns = {@JoinColumn(name = "role_id")})
    private Collection<Role> roles;

    @OneToOne(mappedBy = "user", cascade = CascadeType.ALL)
    @PrimaryKeyJoinColumn
    private LogIn login;

    @OneToMany(
            mappedBy = "user",
            cascade = CascadeType.ALL,
/**
 *
 * The orphanRemoval property of @OneToMany annotation. If it is set to true, then if a parent is deleted in a bi-directional relationship, Hibernate automatically deletes it's children.
 *
 **/
            orphanRemoval = true
    )
    private List<PharmacyGeolocation> pharmacyGeolocationList;

    //Constructors, getters and setters removed for brevity
/**   The parent entity, User, features two utility methods (e.g. addComment and removeComment) which are used to synchronize both sides of the bidirectional association. You should always provide these methods whenever you are working with a bidirectional association as, otherwise, you risk very subtle state propagation issues.
**/

    public void addPharmacyGeolocation(PharmacyGeolocation pharmacyGeolocation) {
        pharmacyGeolocationList.add(pharmacyGeolocation);
//        pharmacyGeolocation.setLatitude(this.);
//        pharmacyGeolocation.setLongitude(this);
    }

    public void removePharmacyGeolocation(PharmacyGeolocation pharmacyGeolocation) {
        pharmacyGeolocationList.remove(pharmacyGeolocation);
        pharmacyGeolocation.setLat(null).setLon(null);
    }

    @OneToMany(
            mappedBy = "user",
            cascade = CascadeType.ALL,
/**
 *
 * The orphanRemoval property of @OneToMany annotation. If it is set to true, then if a parent is deleted in a bi-directional relationship, Hibernate automatically deletes it's children.
 *
 **/
            orphanRemoval = true
    )
    private List<Pharmacy> pharmacy;

    @OneToOne(mappedBy = "user", cascade = CascadeType.ALL)
    @PrimaryKeyJoinColumn
    private UserGeolocation userGeolocation;

    public String getFullName() {
        return firstName != null ? firstName.concat(" ").concat(lastName) : "";
    }
}

