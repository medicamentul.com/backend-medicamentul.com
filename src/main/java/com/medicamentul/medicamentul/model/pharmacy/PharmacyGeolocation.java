package com.medicamentul.medicamentul.model.pharmacy;


//import com.medicamentul.medicamentul.model.pharmacy.Pharmacy;
import com.medicamentul.medicamentul.model.user.User;
import lombok.*;
import lombok.experimental.Accessors;

import javax.persistence.*;

// In addition to mapping the object to the database table, Hibernate can handle mappings of new types
// to the database. The geolocation can be specified as its own table, or it can be normalized, or can have a
// custom serialization mechanism such that you can store it in whatever native form you need.
@Data
@Accessors(chain = true)
@Entity(name = "pharmacy_geolocation")
public class PharmacyGeolocation {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private Double lat;
    private Double lon;


    @OneToOne(mappedBy = "pharmacyGeolocation")
    private Pharmacy pharmacy;

    @ManyToOne(fetch = FetchType.LAZY)
    private User user;




}
