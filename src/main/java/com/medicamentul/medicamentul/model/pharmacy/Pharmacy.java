package com.medicamentul.medicamentul.model.pharmacy;


import com.medicamentul.medicamentul.model.user.User;

import lombok.*;
import lombok.experimental.Accessors;

import javax.persistence.*;

@Data
@Accessors(chain = true)
@Entity(name = "pharmacies")
public class Pharmacy {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private String name;
    private String schedule;
    private String address;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "pharmacy_geolocation_id", referencedColumnName = "id")
    private PharmacyGeolocation pharmacyGeolocation;

    @ManyToOne(fetch = FetchType.LAZY)
    private User user;
}
