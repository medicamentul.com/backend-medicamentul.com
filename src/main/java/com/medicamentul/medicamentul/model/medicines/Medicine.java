package com.medicamentul.medicamentul.model.medicines;


import lombok.*;
import lombok.experimental.Accessors;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.LocalDateTime;
import java.util.Date;

@Data
@Accessors(chain = true)
@Entity(name = "medicines")
public class Medicine {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO, generator="native")
    @GenericGenerator(name = "native", strategy = "native")
//    @Id
//    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private String codIdentificareMedicament;
    private String denumireComerciala;
    private String denumireComercialaInternationala;
    private String formaFarmaceutica;
    private String concentratie;
    private String firmaTaraProducatoare;
    private String firmaTaraDetinatoare;
    private String clasificareAnatomicaTerapeuticaChimica;
    private String actiuneTerapeutica;
    private String prescriptie;
    private String dataAmbalaj;
    private String ambalaj;
    private String volumAmbalaj;
    private String valabilitateAmbalaj;
    private String bulina;
    private String diez;
    private String stea;
    private String triunghi;
    private String dreptunghi;
    private String dataActualizare;





}
