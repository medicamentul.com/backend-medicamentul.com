package com.medicamentul.medicamentul.model.medical_devices;


import lombok.Data;
import lombok.experimental.Accessors;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Data
@Accessors(chain = true)
@Entity(name = "medical_devices")
public class MedicalDevice {

//    @Id
//    @GeneratedValue(strategy =
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO, generator="native")
    @GenericGenerator(name = "native", strategy = "native")
    private long id;

    private String dispozitivId;
    private String denumireDispozitiv;
    private String tip;
    private String clasa;
    private String codInregistrare;
    private String data;
    private String denumireFirmaProducatoare;
    private String denumireFirmaReprezentantiAutorizati;

}
