package com.medicamentul.medicamentul.service.userService;

import com.medicamentul.medicamentul.dto.mapper.UserMapper;
import com.medicamentul.medicamentul.dto.user.UserDto;
import com.medicamentul.medicamentul.exception.*;
import com.medicamentul.medicamentul.model.user.Role;
import com.medicamentul.medicamentul.model.user.User;
import com.medicamentul.medicamentul.model.user.UserRoles;
import com.medicamentul.medicamentul.repository.user.RoleRepositoryDao;
import com.medicamentul.medicamentul.repository.user.UserRepositoryDao;

import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;



import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.medicamentul.medicamentul.exception.ExceptionType.ENTITY_NOT_FOUND;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final BCryptPasswordEncoder bCryptPasswordEncoder;
    private final RoleRepositoryDao roleRepository;
    private final UserRepositoryDao userRepositoryDao;
    private final ModelMapper modelMapper;

    // Defining a constructor for automatic integration of tests
    // In this way we haven't put the annotation @Autowired on above fields but we "connect them" by constructor, for testing purpose it's better this way
    //No need for the constructor because we used  @RequiredArgsConstructor
//    @Autowired
//    public UserServiceImpl(BCryptPasswordEncoder encoder, RoleRepositoryDao roleRepositoryDao,
//                           UserRepositoryDao userRepositoryDao, ModelMapper modelMapper) {
//        this.bCryptPasswordEncoder = encoder;
//        this.roleRepository = roleRepositoryDao;
//        this.userRepositoryDao = userRepositoryDao;
//        this.modelMapper = modelMapper;
//    }

    @Override
    public UserDto signup(UserDto userDto) {
        Role userRole;
        User user = userRepositoryDao.findByEmail(userDto.getEmail());
        if (user == null) {
            if (userDto.isAdmin()) {
                userRole = roleRepository.findByRole(UserRoles.ADMIN);
            } else {
                userRole = roleRepository.findByRole(UserRoles.USER);
            }
            user = new User()
                    .setFirstName(userDto.getFirstName())
                    .setLastName(userDto.getLastName())
                    .setRoles(new HashSet<>(Arrays.asList(userRole)))
                    .setAge(userDto.getAge())
                    .setEmail(userDto.getEmail())
                    .setPassword(bCryptPasswordEncoder.encode(userDto.getPassword()));

            return UserMapper.toUserDto(userRepositoryDao.save(user));
        }
        throw exception(EntityType.USER, ExceptionType.DUPLICATE_ENTITY, userDto.getEmail());
    }

    /**
     * Search an existing user
     *
     * @param email
     * @return
     */
    /**
     * Optional class is a container for non null objects.This class has various utility methods to facilitate code to handle values as ‘available’ or ‘not available’ instead of checking null values.
     */
    @Transactional
    public UserDto findUserByEmail(String email) {
        Optional<User> user = Optional.ofNullable(userRepositoryDao.findByEmail(email));
        if (user.isPresent()) {
            return modelMapper.map(user.get(), UserDto.class);
        }
        throw exception(EntityType.USER, ExceptionType.ENTITY_NOT_FOUND, email);
    }

    /**
     * Update User Profile
     *
     * @param userDto
     * @return
     */
    @Override
    public UserDto updateProfile(UserDto userDto) {
        Optional<User> user = Optional.ofNullable(userRepositoryDao.findByEmail(userDto.getEmail()));
        if (user.isPresent()) {
            User userModel = user.get();
            userModel.setFirstName(userDto.getFirstName())
                     .setLastName(userDto.getLastName())
                     .setFirstName(userDto.getFirstName())
                     .setAge(userDto.getAge())
                     .setEmail(userDto.getEmail());
            return UserMapper.toUserDto(userRepositoryDao.save(userModel));
        }
        throw exception(EntityType.USER, ExceptionType.ENTITY_NOT_FOUND, userDto.getEmail());
    }

    /**
     * Change Password
     *
     * @param userDto
     * @param newPassword
     * @return
     */
    @Override
    public UserDto changePassword(UserDto userDto, String newPassword) {
        Optional<User> user = Optional.ofNullable(userRepositoryDao.findByEmail(userDto.getEmail()));
        if (user.isPresent()) {
            User userModel = user.get();
            userModel.setPassword(bCryptPasswordEncoder.encode(newPassword));
            return UserMapper.toUserDto(userRepositoryDao.save(userModel));
        }
        throw exception(EntityType.USER, ENTITY_NOT_FOUND, userDto.getEmail());
    }

    /**
     * Returns a new RuntimeException
     *
     * @param entityType
     * @param exceptionType
     * @param args
     * @return
     */
    private RuntimeException exception(EntityType entityType, ExceptionType exceptionType, String... args) {
        return MdsException.throwException(entityType, exceptionType, args);
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        User existingUser = userRepositoryDao.findByEmail(email);

        if(existingUser == null) {
            throw new UsernameNotFoundException("Invalid user name or password provided");
        }

        return new org.springframework.security.core.userdetails.User(
                existingUser.getEmail(),
                existingUser.getPassword(),
                mapRolesToAuthorities(existingUser.getRoles())
        );
    }

    private Collection<? extends GrantedAuthority> mapRolesToAuthorities(Collection<Role> roles) {
        return roles.stream()
                .map(role -> new SimpleGrantedAuthority(role.getRole().toString()))
                .collect(Collectors.toList());
    }
}
