package com.medicamentul.medicamentul.service.medicineService;

import com.medicamentul.medicamentul.dto.mapper.MedicineMapper;
import com.medicamentul.medicamentul.dto.model.MedicineDto;
import com.medicamentul.medicamentul.model.medicines.Medicine;
import com.medicamentul.medicamentul.repository.medicines.MedicineRepositoryDao;
import com.medicamentul.medicamentul.util.ExcelHelper;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@Service
@RequiredArgsConstructor
public class MedicineServiceImpl {

    private final MedicineRepositoryDao medicineRepositoryDao;
    private final ModelMapper modelMapper;

    //No need for constructor because I use @RequiredArgsConstructor and the beans are initialized that way

    public void saveExcel(MultipartFile file) {
        try {
            List<Medicine> medicines = ExcelHelper.excelToMedicine(file.getInputStream());
            medicineRepositoryDao.saveAll(medicines);
        } catch (IOException e) {
            throw new RuntimeException("fail to store excel data: " + e.getMessage());
        }
    }

    public List<MedicineDto> findMedicineInfoForNonUser(String searchMedicine) {

        List<Medicine> medicines = medicineRepositoryDao.findTop17ByDenumireComercialaContaining(searchMedicine);
        List<MedicineDto> medicineDtos = medicines
                .stream()
                .map(medicine -> modelMapper.map(MedicineMapper.toMedicinesDtoForNonUser(medicine), MedicineDto.class))
                .collect(Collectors.toList());


        return medicineDtos;
    }

    public List<MedicineDto> findMedicineInfoForUser(String searchMedicine) {

        List<Medicine> medicines = medicineRepositoryDao.findByDenumireComercialaContaining(searchMedicine);
        List<MedicineDto> medicineDtos = medicines
                .stream()
                .map(medicine -> modelMapper.map(MedicineMapper.toMedicinesDtoForUser(medicine), MedicineDto.class))
                .collect(Collectors.toList());

        return medicineDtos;
    }

    public HashMap<Long, String> getCollectionNameAndIdsForAnguarSearchForm() {
        List<Medicine> allMedicines = medicineRepositoryDao.findAll();

        HashMap<Long, String> listByNameOfAllMedicinesForAngularSearchForm = new HashMap<>();
        allMedicines
                .stream()
                .forEach(medicine -> listByNameOfAllMedicinesForAngularSearchForm.put(medicine.getId(), medicine.getDenumireComerciala()));

        return listByNameOfAllMedicinesForAngularSearchForm;
    }

}
