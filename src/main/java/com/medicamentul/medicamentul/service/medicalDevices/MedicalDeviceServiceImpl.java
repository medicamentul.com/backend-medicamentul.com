package com.medicamentul.medicamentul.service.medicalDevices;


import com.medicamentul.medicamentul.dto.mapper.MedicalDeviceMapper;
import com.medicamentul.medicamentul.dto.model.MedicalDeviceDto;
import com.medicamentul.medicamentul.model.medical_devices.MedicalDevice;
import com.medicamentul.medicamentul.repository.medicalDevices.MedicalDeviceRepositoryDao;
import com.medicamentul.medicamentul.util.ExcelHelper;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@Service
@RequiredArgsConstructor
public class MedicalDeviceServiceImpl {


    private final MedicalDeviceRepositoryDao medicalDeviceRepositoryDao;
    private final ModelMapper modelMapper;


    //No need for constructor because I use @RequiredArgsConstructor and the beans are initialized that way

    public void saveMedicalDevicesExcel(MultipartFile file) {
        try {
            List<MedicalDevice> medicalDevices = ExcelHelper.excelToMedicalDevices(file.getInputStream());
            medicalDeviceRepositoryDao.saveAll(medicalDevices);
        } catch (IOException e) {
            throw new RuntimeException("fail to store excel data: " + e.getMessage());
        }
    }

    public List<MedicalDeviceDto> findMedicalDeviceInfoForNonUser(String searchMedicalDevice) {

        List<MedicalDevice> medicalDevices = medicalDeviceRepositoryDao.findTop17ByDenumireDispozitivContaining(searchMedicalDevice);
        List<MedicalDeviceDto> medicalDeviceDtos = medicalDevices
                .stream()
                .map(medicalDevice -> modelMapper.map(MedicalDeviceMapper.toMedicalDevicesDtoForNonUser(medicalDevice), MedicalDeviceDto.class))
                .collect(Collectors.toList());


        return medicalDeviceDtos;
    }

    public List<MedicalDeviceDto> findMedicalDeviceInfoForUser(String searchMedicalDevice) {

        List<MedicalDevice> medicalDevices = medicalDeviceRepositoryDao.findByDenumireDispozitivContaining(searchMedicalDevice);
        List<MedicalDeviceDto> medicalDeviceDtos = medicalDevices
                .stream()
                .map(medicalDevice -> modelMapper.map(MedicalDeviceMapper.toMedicalDevicesDtoForUser(medicalDevice), MedicalDeviceDto.class))
                .collect(Collectors.toList());

        return medicalDeviceDtos;
    }

    public HashMap<Long, String> getCollectionNameAndIdsForAnguarSearchForm() {
        List<MedicalDevice> allMedicalDevices = medicalDeviceRepositoryDao.findAll();

        HashMap<Long, String> listByNameOfAllMedicalDevicesForAngularSearchForm = new HashMap<>();
        allMedicalDevices
                .stream()
                .forEach(medicalDevice -> listByNameOfAllMedicalDevicesForAngularSearchForm.put(medicalDevice.getId(), medicalDevice.getDenumireDispozitiv()));

        return listByNameOfAllMedicalDevicesForAngularSearchForm;
    }
}
