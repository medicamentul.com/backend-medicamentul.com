package com.medicamentul.medicamentul.dto.model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.medicamentul.medicamentul.model.pharmacy.PharmacyGeolocation;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@ToString
@JsonInclude(value = JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class PharmacyDto {


    private String name;
    private String schedule;
    private String address;
//    private PharmacyGeolocation pharmacyGeolocation;

}
