package com.medicamentul.medicamentul.dto.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@ToString
@JsonInclude(value = JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class MedicalDeviceDto {

    @JsonIgnore// this annotation dose not allow field id to pe passed to JSON
    private long id;
    private String dispozitivId;
    private String denumireDispozitiv;
    private String tip;
    private String clasa;
    private String codInregistrare;
    private String data;
    private String denumireFirmaProducatoare;
    private String denumireFirmaReprezentantiAutorizati;
}
