package com.medicamentul.medicamentul.dto.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;
import java.util.Date;

@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@ToString
@JsonInclude(value = JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class MedicineDto {

    @JsonIgnore// this annotation dose not allow field id to pe passed to JSON
    private Long id;
    private String codIdentificareMedicament;
    private String denumireComerciala;
    private String denumireComercialaInternationala;
    private String formaFarmaceutica;
    private String concentratie;
    private String firmaTaraProducatoare;
    private String firmaTaraDetinatoare;
    private String clasificareAnatomicaTerapeuticaChimica;
    private String actiuneTerapeutica;
    private String prescriptie;
    private String dataAmbalaj;
    private String ambalaj;
    private String volumAmbalaj;
    private String valabilitateAmbalaj;
    private String bulina;
    private String diez;
    private String stea;
    private String triunghi;
    private String dreptunghi;
    private String dataActualizare;

}
