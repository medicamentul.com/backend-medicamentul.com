package com.medicamentul.medicamentul.dto.mapper;

import com.medicamentul.medicamentul.dto.model.MedicineDto;
import com.medicamentul.medicamentul.model.medicines.Medicine;
import org.springframework.stereotype.Component;

@Component
public class MedicineMapper {
    public static MedicineDto toMedicinesDtoForNonUser(Medicine medicine) {
        return new MedicineDto()
                .setId(medicine.getId())
                .setCodIdentificareMedicament(medicine.getCodIdentificareMedicament())
                .setDenumireComerciala(medicine.getDenumireComerciala())
                .setDenumireComercialaInternationala(medicine.getDenumireComercialaInternationala())
                .setFormaFarmaceutica(medicine.getFormaFarmaceutica())
                .setConcentratie(medicine.getConcentratie())
                .setFirmaTaraProducatoare(medicine.getFirmaTaraProducatoare())
                .setFirmaTaraDetinatoare(medicine.getFirmaTaraDetinatoare())
                .setClasificareAnatomicaTerapeuticaChimica(medicine.getClasificareAnatomicaTerapeuticaChimica())
                .setActiuneTerapeutica(medicine.getActiuneTerapeutica())
                .setAmbalaj(medicine.getAmbalaj())
                .setVolumAmbalaj(medicine.getVolumAmbalaj())
                .setDataActualizare(medicine.getDataActualizare());
    }

    public static MedicineDto toMedicinesDtoForUser(Medicine medicine) {
        return new MedicineDto()
                .setCodIdentificareMedicament(medicine.getCodIdentificareMedicament())
                .setDenumireComerciala(medicine.getDenumireComerciala())
                .setDenumireComercialaInternationala(medicine.getDenumireComercialaInternationala())
                .setFormaFarmaceutica(medicine.getFormaFarmaceutica())
                .setConcentratie(medicine.getConcentratie())
                .setFirmaTaraProducatoare(medicine.getFirmaTaraProducatoare())
                .setFirmaTaraDetinatoare(medicine.getFirmaTaraDetinatoare())
                .setClasificareAnatomicaTerapeuticaChimica(medicine.getClasificareAnatomicaTerapeuticaChimica())
                .setActiuneTerapeutica(medicine.getActiuneTerapeutica())
                .setPrescriptie(medicine.getPrescriptie())
                .setDataAmbalaj(medicine.getDataAmbalaj())
                .setAmbalaj(medicine.getAmbalaj())
                .setVolumAmbalaj(medicine.getVolumAmbalaj())
                .setValabilitateAmbalaj(medicine.getValabilitateAmbalaj())
                .setBulina(medicine.getBulina())
                .setDiez(medicine.getDiez())
                .setStea(medicine.getStea())
                .setTriunghi(medicine.getTriunghi())
                .setDreptunghi(medicine.getDreptunghi())
                .setDataActualizare(medicine.getDataActualizare());
    }
}
