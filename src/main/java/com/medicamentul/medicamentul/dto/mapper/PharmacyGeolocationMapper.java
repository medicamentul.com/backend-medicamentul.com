package com.medicamentul.medicamentul.dto.mapper;

import com.medicamentul.medicamentul.dto.model.PharmacyGeolocationDto;
import com.medicamentul.medicamentul.model.pharmacy.PharmacyGeolocation;


public class PharmacyGeolocationMapper {

    public static PharmacyGeolocationDto toGeolocationDto(PharmacyGeolocation pharmacyGeolocation) {
        return new PharmacyGeolocationDto()
                .setLat(pharmacyGeolocation.getLat())
                .setLon(pharmacyGeolocation.getLon());
    }
}
