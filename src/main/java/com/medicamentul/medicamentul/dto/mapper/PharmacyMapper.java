package com.medicamentul.medicamentul.dto.mapper;

import com.medicamentul.medicamentul.dto.model.PharmacyDto;
import com.medicamentul.medicamentul.model.pharmacy.Pharmacy;


public class PharmacyMapper {

    public static PharmacyDto toPharmacyDto(Pharmacy pharmacy) {
        return new PharmacyDto()
//                .setId(pharmacy.getId())
                .setName(pharmacy.getName())
                .setSchedule(pharmacy.getSchedule())
                .setAddress(pharmacy.getAddress());
    }
}
