package com.medicamentul.medicamentul.dto.mapper;

import com.medicamentul.medicamentul.dto.model.MedicalDeviceDto;
import com.medicamentul.medicamentul.model.medical_devices.MedicalDevice;
import org.springframework.stereotype.Component;

@Component
public class MedicalDeviceMapper {

    public static MedicalDeviceDto toMedicalDevicesDtoForNonUser(MedicalDevice medicalDevice) {
        return new MedicalDeviceDto()
                .setId(medicalDevice.getId())
                .setDispozitivId(medicalDevice.getDispozitivId())
                .setDenumireDispozitiv(medicalDevice.getDenumireDispozitiv())
                .setTip(medicalDevice.getTip())
                .setClasa(medicalDevice.getClasa())
                .setCodInregistrare(medicalDevice.getCodInregistrare())
                .setData(medicalDevice.getData())
                .setDenumireFirmaProducatoare(medicalDevice.getDenumireFirmaProducatoare())
                .setDenumireFirmaReprezentantiAutorizati(medicalDevice.getDenumireFirmaReprezentantiAutorizati());
    }

    public static MedicalDeviceDto toMedicalDevicesDtoForUser(MedicalDevice medicalDevice) {
        return new MedicalDeviceDto()
                .setId(medicalDevice.getId())
                .setDispozitivId(medicalDevice.getDispozitivId())
                .setDenumireDispozitiv(medicalDevice.getDenumireDispozitiv())
                .setTip(medicalDevice.getTip())
                .setClasa(medicalDevice.getClasa())
                .setCodInregistrare(medicalDevice.getCodInregistrare())
                .setData(medicalDevice.getData())
                .setDenumireFirmaProducatoare(medicalDevice.getDenumireFirmaProducatoare())
                .setDenumireFirmaReprezentantiAutorizati(medicalDevice.getDenumireFirmaReprezentantiAutorizati());
    }
}
