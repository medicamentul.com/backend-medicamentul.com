package com.medicamentul.medicamentul.dto.mapper;


import com.medicamentul.medicamentul.dto.user.RoleDto;
import com.medicamentul.medicamentul.dto.user.UserDto;
import com.medicamentul.medicamentul.model.user.User;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.stream.Collectors;

@Component
public class UserMapper {
    public static UserDto toUserDto(User user) {
        return new UserDto()
                .setId(user.getId())
                .setLastName(user.getFirstName())
                .setLastName(user.getLastName())
                .setAge(user.getAge())
                .setEmail(user.getEmail())
                .setPassword(user.getPassword())
                .setRoles(new HashSet<RoleDto>(user
                        .getRoles()
                        .stream()
                        .map(role -> new ModelMapper().map(role, RoleDto.class))
                        .collect(Collectors.toSet())));
    }
}