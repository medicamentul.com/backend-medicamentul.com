package com.medicamentul.medicamentul.dto.mapper;

import com.medicamentul.medicamentul.dto.user.UserGeolocationDto;
import com.medicamentul.medicamentul.model.user.UserGeolocation;

public class UserGeolocationMapper {

    public static UserGeolocationDto toUserGeolocationDto(UserGeolocation userGeolocation) {
        return new UserGeolocationDto()
                .setId(userGeolocation.getId())
                .setLat(userGeolocation.getLat())
                .setLon(userGeolocation.getLon())
                .setUser(userGeolocation.getUser().getFullName());
    }


}
