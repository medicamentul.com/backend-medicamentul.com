//package com.medicamentul.medicamentul.dto.mapper;
//
//import com.medicamentul.medicamentul.dto.model.MedicineDto;
//import com.medicamentul.medicamentul.model.medicines.Medicine;
//import org.springframework.stereotype.Component;
//
//import java.util.Date;
//
//@Component
//public class MedicineMapperForUser {
//
//    public static MedicineDto toMedicinesDtoForUsers(Medicine medicine) {
//        return new MedicineDto()
//                .setCodIdentificareMedicament(medicine.getCodIdentificareMedicament())
//                .setDenumireComerciala(medicine.getDenumireComerciala())
//                .setDenumireComercialaInternationala(medicine.getDenumireComercialaInternationala())
//                .setFormaFarmaceutica(medicine.getFormaFarmaceutica())
//                .setConcentratie(medicine.getConcentratie())
//                .setFirmaTaraProducatoare(medicine.getFirmaTaraProducatoare())
//                .setFirmaTaraDetinatoare(medicine.getFirmaTaraDetinatoare())
//                .setClasificareAnatomicaTerapeuticaChimica(medicine.getClasificareAnatomicaTerapeuticaChimica())
//                .setActiuneTerapeutica(medicine.getActiuneTerapeutica())
//                .setPrescriptie(medicine.getPrescriptie())
//                .setDataAmbalaj(medicine.getDataAmbalaj())
//                .setAmbalaj(medicine.getAmbalaj())
//                .setVolumAmbalaj(medicine.getVolumAmbalaj())
//                .setValabilitateAmbalaj(medicine.getValabilitateAmbalaj())
//                .setBulina(medicine.getBulina())
//                .setDiez(medicine.getDiez())
//                .setStea(medicine.getStea())
//                .setTriunghi(medicine.getTriunghi())
//                .setDreptunghi(medicine.getDreptunghi())
//                .setDataActualizare(medicine.getDataActualizare());
//    }
//}
