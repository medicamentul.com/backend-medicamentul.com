package com.medicamentul.medicamentul.controller.v1.api;


import com.medicamentul.medicamentul.controller.v1.request.UserSignupRequest;

import com.medicamentul.medicamentul.dto.response.Response;
import com.medicamentul.medicamentul.dto.user.UserDto;
import com.medicamentul.medicamentul.service.userService.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.Api;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;


@RestController
@CrossOrigin("http://localhost:4201")
@RequestMapping("/api/v1/user")
@Api(value = "mds-application", description = "Operations pertaining to user management in the Mds application")
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;


    @GetMapping(value = {"/login"})
    public ModelAndView login() {
        return new ModelAndView("login");
    }

    @GetMapping(value = {"/logout"})
    public String logout() {
        SecurityContextHolder.getContext().setAuthentication(null);
        return "redirect:home";
    }

    /**
     * Handles the incoming POST API "/v1/user/signup"
     *
     * @param userSignupRequest
     * @return
     */

    @PostMapping("/signup")
    public Response signup(@RequestBody @Valid UserSignupRequest userSignupRequest) {
        return Response.ok().setPayload(registerUser(userSignupRequest, false));
    }

    /**
     * Register a new user in the database
     *
     * @param userSignupRequest
     * @return
     **/
    private UserDto registerUser(UserSignupRequest userSignupRequest, boolean isAdmin) {
        UserDto userDto = new UserDto()
                .setFirstName(userSignupRequest.getFirstName())
                .setLastName(userSignupRequest.getLastName())
                .setAge(userSignupRequest.getAge())
                .setEmail(userSignupRequest.getEmail())
                .setPassword(userSignupRequest.getPassword())
                .setAdmin(isAdmin);

        return userService.signup(userDto);
    }

}
