package com.medicamentul.medicamentul.controller.v1.api;

import com.medicamentul.medicamentul.dto.model.MedicalDeviceDto;
import com.medicamentul.medicamentul.exception.ResponseMessage;
import com.medicamentul.medicamentul.service.medicalDevices.MedicalDeviceServiceImpl;
import com.medicamentul.medicamentul.util.ExcelHelper;
import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@CrossOrigin("http://localhost:4201")
@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class MedicalDevicesController {

    private final MedicalDeviceServiceImpl medicalDeviceService;

    @PostMapping("/excel/upload_medical_devices")
    public ResponseEntity<ResponseMessage> uploadMedicalDeviceFile(@RequestParam("file") MultipartFile file) {

        String message = "";
//        final Logger logger =
//                LogManager.getLogger(MedicalDeviceServiceImpl.class.getName());

        if (ExcelHelper.hasExcelFormat(file)) {
            try {
                medicalDeviceService.saveMedicalDevicesExcel(file);

                message = "Uploaded the file successfully: " + file.getOriginalFilename();
                return ResponseEntity.status(HttpStatus.OK).body(new ResponseMessage(message));

            } catch (Exception e) {
//                logger.fatal("Unable to open file.", e);
                message = "Could not upload the file: " + file.getOriginalFilename() + "!";
                return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new ResponseMessage(message));
            }
        }

        message = "Please upload an excel file!";
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ResponseMessage(message));
    }

    //@GetMapping("/search?field={}&value={}")
    @GetMapping("/search_medical_device")
    public ResponseEntity<List<MedicalDeviceDto>> searchMedicalDeviceForNonUser(@RequestParam(value = "medicalDevice") String searchMedicalDevice) {
        if (searchMedicalDevice == null || searchMedicalDevice == "") {
            throw new IllegalArgumentException("{\"error\":\"Empty paraemter value. Not allowed!\"}");
        }

        final List<MedicalDeviceDto> medicalDeviceDtos = medicalDeviceService.findMedicalDeviceInfoForNonUser(searchMedicalDevice);

        return ResponseEntity.status(HttpStatus.OK).body(medicalDeviceDtos);

    }

    @GetMapping("dashboard/#search_medical_devices")
    public ResponseEntity<List<MedicalDeviceDto>> searchMedicalDeviceForUser(@RequestParam(value = "medical_device") String searchMedicalDevice) {

        final List<MedicalDeviceDto> medicalDeviceDtos = medicalDeviceService.findMedicalDeviceInfoForUser(searchMedicalDevice);

        return ResponseEntity.status(HttpStatus.OK).body(medicalDeviceDtos);
    }

    @ExceptionHandler(IllegalArgumentException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public final String exceptionHandlerIllegalArgumentException(final IllegalArgumentException e) {
        return '"' + e.getMessage() + '"';
    }
}
