package com.medicamentul.medicamentul.controller.v1.ui;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin("http://localhost:4200")
@RestController
public class HomeController {

    @GetMapping(value = {"/", "/home"})
    public String home() {
        return "home";
    }
}
