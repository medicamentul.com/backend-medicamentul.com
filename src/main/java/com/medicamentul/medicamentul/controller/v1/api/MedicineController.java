package com.medicamentul.medicamentul.controller.v1.api;

import com.medicamentul.medicamentul.dto.model.MedicineDto;
import com.medicamentul.medicamentul.exception.ResponseMessage;

import com.medicamentul.medicamentul.service.medicineService.MedicineServiceImpl;
import com.medicamentul.medicamentul.util.ExcelHelper;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import sun.swing.SwingUtilities2;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.toMap;


@CrossOrigin("http://localhost:4201")
@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class MedicineController {



    private final MedicineServiceImpl medicineService;


    @PostMapping("/excel/upload")
    public ResponseEntity<ResponseMessage> uploadMedicineFile(@RequestParam("file") MultipartFile file) {

        String message = "";

        if (ExcelHelper.hasExcelFormat(file)) {
            try {
                medicineService.saveExcel(file);

                message = "Uploaded the file successfully: " + file.getOriginalFilename();
                return ResponseEntity.status(HttpStatus.OK).body(new ResponseMessage(message));

            } catch (Exception e) {
                message = "Could not upload the file: " + file.getOriginalFilename() + "!";
                return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new ResponseMessage(message));
            }
        }

        message = "Please upload an excel file!";
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ResponseMessage(message));
    }

    //@GetMapping("/search?field={}&value={}")
    @GetMapping("/search")
    public ResponseEntity<List<MedicineDto>> searchMedicineForNonUser(@RequestParam(value = "medicine") String searchMedicine) {
        if (null == searchMedicine || "" == searchMedicine) {
            throw new IllegalArgumentException("{\"error\":\"Empty paraemter value. Not allowed!\"}");
        }

        final List<MedicineDto> medicines = medicineService.findMedicineInfoForNonUser(searchMedicine);

        return ResponseEntity.status(HttpStatus.OK).body(medicines);
    }

    @GetMapping("dashboard/#search")
    public ResponseEntity<List<MedicineDto>> searchMedicineForUser(@RequestParam(value = "medicine") String searchMedicine) {

        final List<MedicineDto> medicines = medicineService.findMedicineInfoForUser(searchMedicine);

        return ResponseEntity.status(HttpStatus.OK).body(medicines);
    }

    @ExceptionHandler(IllegalArgumentException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public final String exceptionHandlerIllegalArgumentException(final IllegalArgumentException e) {
        return '"' + e.getMessage() + '"';
    }



}






