package com.medicamentul.medicamentul.repository.medicines;

import com.medicamentul.medicamentul.model.medicines.Medicine;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface MedicineRepositoryDao extends JpaRepository<Medicine, Long> {

    List<Medicine> findTop17ByDenumireComercialaContaining(String denumireComerciala);

    List<Medicine> findByDenumireComercialaContaining(String title);

}
