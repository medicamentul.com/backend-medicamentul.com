package com.medicamentul.medicamentul.repository.user;

import com.medicamentul.medicamentul.model.user.UserGeolocation;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserGeolocationRepositoryDao extends JpaRepository<UserGeolocation, Long> {

    UserGeolocation findByLatAndLon(Double lat, Double lon);
}
