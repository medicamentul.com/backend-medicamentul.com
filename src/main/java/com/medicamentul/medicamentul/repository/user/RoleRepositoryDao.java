package com.medicamentul.medicamentul.repository.user;

import com.medicamentul.medicamentul.model.user.Role;
import com.medicamentul.medicamentul.model.user.UserRoles;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface RoleRepositoryDao extends JpaRepository<Role, Long> {

    Role findByRole(UserRoles role);

}
