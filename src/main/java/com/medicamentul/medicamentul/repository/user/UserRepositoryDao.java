package com.medicamentul.medicamentul.repository.user;

import com.medicamentul.medicamentul.model.user.User;
import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.stereotype.Repository;


@Repository
public interface UserRepositoryDao extends JpaRepository<User, Long> {

    User findByEmail(String email);

}
