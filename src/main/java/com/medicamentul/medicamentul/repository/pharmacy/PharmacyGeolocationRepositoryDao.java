package com.medicamentul.medicamentul.repository.pharmacy;

import com.medicamentul.medicamentul.model.pharmacy.PharmacyGeolocation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PharmacyGeolocationRepositoryDao extends JpaRepository<PharmacyGeolocation, Long> {

}
