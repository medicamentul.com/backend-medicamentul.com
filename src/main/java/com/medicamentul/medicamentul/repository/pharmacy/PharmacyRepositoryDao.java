package com.medicamentul.medicamentul.repository.pharmacy;

import com.medicamentul.medicamentul.model.pharmacy.Pharmacy;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface PharmacyRepositoryDao extends JpaRepository<Pharmacy, Long> {




}
