package com.medicamentul.medicamentul.repository.medicalDevices;

import com.medicamentul.medicamentul.model.medical_devices.MedicalDevice;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MedicalDeviceRepositoryDao extends JpaRepository<MedicalDevice, Long> {

    List<MedicalDevice> findTop17ByDenumireDispozitivContaining(String denumireDispozitiv);

    List<MedicalDevice> findByDenumireDispozitivContaining(String title);
}
