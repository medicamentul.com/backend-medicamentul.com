package com.medicamentul.medicamentul;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


//Povide custom test behavior, JUnit is given SpringRunner, a Spring-provided test runner that provides for the creation of a Spring application context that the test will run against
@RunWith(SpringRunner.class)
// @SpringBootTest - tells JUnit to bootstrap the test with Spring Boot capabilities
// @SpringBootTest - Is the test class equivalent of calling SpringApplication.run() in a main() method
@SpringBootTest
class MedicamentulApplicationTests {

    @Test
    void contextLoads() {
    }

}
